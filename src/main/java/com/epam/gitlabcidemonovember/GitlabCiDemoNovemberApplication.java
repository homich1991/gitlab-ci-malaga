package com.epam.gitlabcidemonovember;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCiDemoNovemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabCiDemoNovemberApplication.class, args);
    }

}
